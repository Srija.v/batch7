def create(value,i,type):
    page = factory()
    words = value.split()
    findMax(len(words))
    page.setList(words)
    if type is 'p':
        page.setName("P" + str(i + 1))
        add_to_global(words, page.getName())
        global_list_of_pages[page.getName()] = page
    else:
        page.setName("Q" + str(i + 1))
        global_list_of_queries.append(page)

def findMax(number):
    global max_weight
    if max_weight < number:
        max_weight = number
        
def readFromFile():
        print("Input read from input.txt file : ")
        f = open('input.txt','r')
        page_index =0
        query_index = 0
        for line in f:
            print(line,end="")
            if line[0] is 'P':
                create(line[1:],page_index,'p')
                page_index += 1

            if line[0] is 'Q':
                create(line[1:],query_index,  6'q')
                query_index += 1
        print("\n")

if __name__ == '__main__':
    readFromFile()
    assignWeights()
    searchMain()

