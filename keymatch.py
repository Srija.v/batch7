def searchMain(): 
    for query in global_list_of_queries:
        visited =[]
        d ={}
        for word in query.getWordList():
            if word in global_word_list:
                for page in global_word_list[word]:
                    if page not in visited:
                        visited.append(page)
                        sop = sumOfProducts(query,global_list_of_pages[page])
                        d[page] = sop
        d = OrderedDict(sorted(d.items(), key=lambda x: (-x[1], (x[0][0], int(x[0][1:])))))
        forPrint(query.getName(),d)

def sumOfProducts(query,page):   
    sop = 0
    for qword in query.getWordList():
        if qword in page.getWordList():
            sop = sop + query.getWordList()[qword] * page.getWordList()[qword]
    return sop
